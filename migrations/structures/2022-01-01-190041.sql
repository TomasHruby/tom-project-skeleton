SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

ALTER TABLE `address`
ADD COLUMN `note` VARCHAR(1023) NULL AFTER `longitude`,
ADD COLUMN `address_name` VARCHAR(127) NULL AFTER `note`,
ADD COLUMN `active` TINYINT(1) UNSIGNED NOT NULL AFTER `metadata`,
ADD COLUMN `priority` INT(11) NOT NULL AFTER `active`;
