SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

CREATE TABLE `country` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(127) NOT NULL COMMENT 'Name in original language',
	`name_cs` VARCHAR(127) NOT NULL COMMENT 'Czech name',
	`name_en` VARCHAR(127) NOT NULL COMMENT 'English name',
	`state_code` CHAR(2) NOT NULL COMMENT 'ISO 3166-2 alpha 2',
	`state_code_3` CHAR(3) NOT NULL COMMENT 'ISO 3166-2 alpha 3',
	`currency` CHAR(3) NOT NULL COMMENT 'ISO 4217',
	`in_eu` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Is country member of European Union?',
	`in_schengen` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Is country in Schengen economic zone?',
	`phone_prefix` VARCHAR(15) NOT NULL,
	`postal_code_pattern` VARCHAR(1024) NULL,
	`postal_code_example` VARCHAR(31) NULL,
	UNIQUE KEY `state_code` (`state_code`),
	UNIQUE KEY `state_code_3` (`state_code_3`),
	PRIMARY KEY (`id`)
)
CHARACTER SET=utf8mb4
COLLATE=utf8mb4_general_ci
ENGINE=InnoDB;

CREATE TABLE `address` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`country_id` INT(10) UNSIGNED NOT NULL,
	`city` VARCHAR(127) NOT NULL,
	`postal_code` VARCHAR(31) NOT NULL COMMENT 'Postal code or ZIP',
	`street` VARCHAR(255) NOT NULL COMMENT 'Street without number',
	`street_prefix` VARCHAR(31) NULL COMMENT 'Eg. 1er, N, S, W.11',
	`street_suffix` VARCHAR(31) NULL COMMENT 'Street Number, Eg. 273, 11E',
	`house_number` VARCHAR(15) NULL,
	`apartment` VARCHAR(15) NULL COMMENT 'Apartment number if any',
	`district` VARCHAR(255) NULL COMMENT 'District, province, municipality',
	`firstname` VARCHAR(127) NULL,
	`midname` VARCHAR(127) NULL,
	`surname` VARCHAR(127) NULL,
	`company_name` VARCHAR(255) NULL,
	`contact_email` VARCHAR(255) NULL,
	`contact_phone` VARCHAR(31) NULL,
	`latitude` TEXT NULL,
	`longitude` TEXT NULL,
	`metadata` JSON NULL,
	`create_date` DATETIME NULL,
	PRIMARY KEY (`id`),
	KEY `address_fk_country_id` (`country_id`),
	CONSTRAINT `address_fk_country_id` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
)
CHARACTER SET=utf8mb4
COLLATE=utf8mb4_general_ci
ENGINE=InnoDB;
