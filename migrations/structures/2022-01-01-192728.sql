SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

ALTER TABLE `account`
MODIFY COLUMN `timezone` VARCHAR(63) NULL AFTER `status`,
MODIFY COLUMN `metadata` JSON NULL AFTER `login_date`;
