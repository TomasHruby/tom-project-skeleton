SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

CREATE TABLE `product` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(128) NOT NULL,
	`sku` VARCHAR(32) NOT NULL,
	`identifier` VARCHAR(32) NULL,
	`ean` VARCHAR(32) NULL,
	`price` DECIMAL(15, 4) NOT NULL,
	`price_supplier` DECIMAL(15, 4) NULL,
	`price_original` DECIMAL(15, 4) NULL,
	`price_vat` DECIMAL(15, 4) NULL,
	`description` TEXT NULL,
	`short_description` TEXT NULL,
	`create_date` DATETIME NULL,
	`is_active` TINYINT(1) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB
CHARACTER SET=utf8mb4
COLLATE=utf8mb4_general_ci;
