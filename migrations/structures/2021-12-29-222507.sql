SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

CREATE TABLE `organization` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL COMMENT 'Name of organization',
	`slug` VARCHAR(255) NOT NULL COMMENT 'Slug used for url',
	`contact_email` VARCHAR(255) NULL,
	`contact_phone` VARCHAR(31) NULL,
	`website` VARCHAR(255) NULL,
	`reg_number` VARCHAR(31) NULL,
	`vat_number` VARCHAR(31) NULL,
	`metadata` JSON NULL,
	`licence` INT(11) NULL,
	`timezone` VARCHAR(63) NULL,
	`create_date` DATETIME NULL,
	PRIMARY KEY (`id`)
)
CHARACTER SET=utf8mb4
COLLATE=utf8mb4_general_ci
ENGINE=InnoDB;
