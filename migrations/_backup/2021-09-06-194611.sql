SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

ALTER TABLE `country`
ADD UNIQUE KEY `state_code_state_code_3` (`state_code`, `state_code_3`);
