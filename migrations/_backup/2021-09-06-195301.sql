SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

ALTER TABLE `country`
ADD UNIQUE KEY `state_code` (`state_code`),
ADD UNIQUE KEY `state_code_3` (`state_code_3`),
DROP INDEX `state_code_state_code_3`;
