SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

ALTER TABLE `organization`
MODIFY COLUMN `create_date` DATETIME NULL AFTER `licence`,
DROP COLUMN `licence_3`;
