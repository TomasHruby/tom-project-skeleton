SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

ALTER TABLE `organization`
ADD COLUMN `licence_3` INT(11) NULL AFTER `licence`;
