SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

CREATE TABLE `country` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(127) NOT NULL COMMENT 'Name in original language',
	`name_cs` VARCHAR(127) NOT NULL COMMENT 'Czech name',
	`name_en` VARCHAR(127) NOT NULL COMMENT 'English name',
	`state_code` CHAR(2) NOT NULL COMMENT 'ISO 3166-2 alpha 2',
	`state_code_3` CHAR(3) NOT NULL COMMENT 'ISO 3166-2 alpha 3',
	`currency` CHAR(3) NOT NULL COMMENT 'ISO 4217',
	`in_eu` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Is country member of European Union?',
	`in_schengen` TINYINT(1) UNSIGNED NOT NULL COMMENT 'Is country in Schengen economic zone?',
	`phone_prefix` VARCHAR(15) NOT NULL,
	`postal_code_pattern` VARCHAR(1024) NULL,
	`postal_code_example` VARCHAR(31) NULL,
	PRIMARY KEY (`id`)
)
CHARACTER SET=utf8mb4
COLLATE=utf8mb4_general_ci
ENGINE=InnoDB;

ALTER TABLE `address`
ADD COLUMN `country_id` INT(10) UNSIGNED NOT NULL AFTER `id`,
ADD COLUMN `city` VARCHAR(127) NOT NULL AFTER `country_id`,
ADD COLUMN `postal_code` VARCHAR(31) NOT NULL COMMENT 'Postal code or ZIP' AFTER `city`,
ADD COLUMN `street` VARCHAR(255) NOT NULL COMMENT 'Street without number' AFTER `postal_code`,
ADD COLUMN `street_prefix` VARCHAR(31) NULL COMMENT 'Eg. 1er, N, S, W.11' AFTER `street`,
ADD COLUMN `street_suffix` VARCHAR(31) NULL COMMENT 'Street Number, Eg. 273, 11E' AFTER `street_prefix`,
ADD COLUMN `house_number` VARCHAR(15) NULL AFTER `street_suffix`,
ADD COLUMN `apartment` VARCHAR(15) NULL COMMENT 'Apartment number if any' AFTER `house_number`,
ADD COLUMN `district` VARCHAR(255) NULL COMMENT 'District, province, municipality' AFTER `apartment`,
ADD COLUMN `midname` VARCHAR(127) NULL AFTER `firstname`,
ADD COLUMN `company_name` VARCHAR(255) NULL AFTER `surname`,
ADD COLUMN `contact_email` VARCHAR(255) NULL AFTER `company_name`,
ADD COLUMN `contact_phone` VARCHAR(31) NULL AFTER `contact_email`,
ADD COLUMN `latitude` TEXT NULL AFTER `contact_phone`,
ADD COLUMN `longitude` TEXT NULL AFTER `latitude`,
ADD KEY `address_fk_country_id` (`country_id`),
ADD CONSTRAINT `address_fk_country_id` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
MODIFY COLUMN `firstname` VARCHAR(127) NULL AFTER `district`,
MODIFY COLUMN `surname` VARCHAR(127) NULL AFTER `midname`,
MODIFY COLUMN `metadata` JSON NULL AFTER `longitude`,
MODIFY COLUMN `create_date` DATETIME NULL AFTER `metadata`,
DROP INDEX `email`,
DROP INDEX `username`,
DROP COLUMN `username`,
DROP COLUMN `email`,
DROP COLUMN `phone`,
DROP COLUMN `password`,
DROP COLUMN `access`,
DROP COLUMN `timezone`,
DROP COLUMN `email_verify_date`,
DROP COLUMN `phone_verify_date`,
DROP COLUMN `login_date`;

ALTER TABLE `organization`
MODIFY COLUMN `contact_email` VARCHAR(255) NULL AFTER `slug`,
MODIFY COLUMN `contact_phone` VARCHAR(31) NULL AFTER `contact_email`,
MODIFY COLUMN `reg_number` VARCHAR(31) NULL AFTER `website`,
MODIFY COLUMN `vat_number` VARCHAR(31) NULL AFTER `reg_number`,
MODIFY COLUMN `timezone` VARCHAR(63) NULL AFTER `licence`;

ALTER TABLE `user`
ADD COLUMN `midname` VARCHAR(127) NULL AFTER `firstname`,
MODIFY COLUMN `firstname` VARCHAR(127) NULL AFTER `id`,
MODIFY COLUMN `surname` VARCHAR(127) NULL AFTER `midname`,
MODIFY COLUMN `username` VARCHAR(63) NOT NULL AFTER `surname`,
MODIFY COLUMN `email` VARCHAR(255) NOT NULL COMMENT 'Used for login' AFTER `username`,
MODIFY COLUMN `phone` VARCHAR(31) NULL AFTER `email`,
MODIFY COLUMN `password` VARCHAR(255) NULL COMMENT 'Used for login' AFTER `phone`,
MODIFY COLUMN `timezone` VARCHAR(63) NULL AFTER `metadata`;
