SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

CREATE TABLE `address` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`firstname` VARCHAR(64) NULL,
	`surname` VARCHAR(64) NULL,
	`username` VARCHAR(32) NOT NULL,
	`email` VARCHAR(128) NOT NULL COMMENT 'Used for login',
	`phone` VARCHAR(32) NULL,
	`password` VARCHAR(256) NULL COMMENT 'Used for login',
	`access` TINYINT(4) NOT NULL,
	`metadata` JSON NULL,
	`timezone` VARCHAR(64) NULL,
	`create_date` DATETIME NULL,
	`email_verify_date` DATETIME NULL,
	`phone_verify_date` DATETIME NULL,
	`login_date` DATETIME NULL,
	UNIQUE KEY `username` (`username`),
	UNIQUE KEY `email` (`email`),
	PRIMARY KEY (`id`)
)
CHARACTER SET=utf8mb4
COLLATE=utf8mb4_general_ci
ENGINE=InnoDB;

ALTER TABLE `organization`
ADD COLUMN `slug` VARCHAR(255) NOT NULL COMMENT 'Slug used for url' AFTER `name`,
ADD COLUMN `metadata` JSON NULL AFTER `vat_number`,
ADD COLUMN `timezone` VARCHAR(64) NULL AFTER `licence`,
MODIFY COLUMN `name` VARCHAR(255) NOT NULL COMMENT 'Name of organization' AFTER `id`,
MODIFY COLUMN `contact_email` VARCHAR(128) NOT NULL AFTER `slug`,
MODIFY COLUMN `website` VARCHAR(255) NULL AFTER `contact_phone`,
MODIFY COLUMN `reg_number` VARCHAR(32) NULL AFTER `website`,
MODIFY COLUMN `licence` INT(11) NULL AFTER `metadata`,
DROP COLUMN `secretary_firstname`,
DROP COLUMN `secretary_surname`,
DROP COLUMN `is_blocked`,
DROP COLUMN `is_active`;

ALTER TABLE `user`
ADD COLUMN `country` VARCHAR(2) NULL AFTER `timezone`,
ADD COLUMN `language` VARCHAR(2) NULL AFTER `country`,
ADD COLUMN `email_verify_date` DATETIME NULL AFTER `create_date`,
ADD COLUMN `phone_verify_date` DATETIME NULL AFTER `email_verify_date`,
ADD UNIQUE KEY `username_email` (`username`, `email`),
MODIFY COLUMN `login_date` DATETIME NULL AFTER `phone_verify_date`,
DROP INDEX `username`,
DROP COLUMN `verify_date`;

CREATE TABLE `user_to_organization` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(10) UNSIGNED NOT NULL,
	`organization_id` INT(10) UNSIGNED NOT NULL,
	`role` TINYINT(4) NOT NULL,
	`create_date` DATETIME NULL,
	UNIQUE KEY `user_id_organization_id` (`user_id`, `organization_id`),
	PRIMARY KEY (`id`),
	KEY `product_to_store_fk_organization_id` (`organization_id`),
	CONSTRAINT `product_to_store_fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT `product_to_store_fk_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
)
CHARACTER SET=utf8mb4
COLLATE=utf8mb4_general_ci
ENGINE=InnoDB;
