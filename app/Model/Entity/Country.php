<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Model\Entity;

/**
 * @property-read int $id m:schemaPrimary
 * @property      string $name m:schemaType(varchar:127) m:schemaComment(Name in original language)
 * @property      string $nameCs m:schemaType(varchar:127) m:schemaComment(Czech name)
 * @property      string $nameEn m:schemaType(varchar:127) m:schemaComment(English name)
 * @property      string $stateCode m:schemaType(char:2) m:schemaComment(ISO 3166-2 alpha 2)
 * @property      string $stateCode3 m:schemaType(char:3) m:schemaComment(ISO 3166-2 alpha 3)
 * @property      string $currency m:schemaType(char:3) m:schemaComment(ISO 4217)
 * @property      bool $inEu m:default(false) m:schemaDefault(0) m:schemaComment(Is country member of European Union?)
 * @property      bool $inSchengen m:default(false) m:schemaDefault(0) m:schemaComment(Is country in Schengen economic zone?)
 * @property      string $phonePrefix m:schemaType(varchar:15)
 * @property      string|null $postalCodePattern m:schemaType(varchar:1024)
 * @property      string|null $postalCodeExample m:schemaType(varchar:31)
 *
 * @schemaUnique stateCode
 * @schemaUnique stateCode3
 */
class Country extends BaseEntity
{

}
