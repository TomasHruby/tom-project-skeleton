<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */
declare(strict_types=1);

namespace Model\Entity;

abstract class BaseEntity extends \Rockette\Model\Entity\SuperEntity
{

}
