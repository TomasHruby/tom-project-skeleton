<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Model\Entity;

use DateTime;

/**
 * @property int $id m:schemaPrimary
 * @property string $name m:schemaType(varchar:128)
 * @property string $sku m:schemaType(varchar:32)
 * @property string|null $identifier m:schemaType(varchar:32)
 * @property string|null $ean m:schemaType(varchar:32)
 * @property float $price m:schemaType(money)
 * @property float|null $priceSupplier m:schemaType(money)
 * @property float|null $priceOriginal m:schemaType(money)
 * @property float|null $priceVat m:schemaType(money)
 * @property string|null $description
 * @property string|null $shortDescription
 * @property DateTime|null $createDate m:schemaType(DateTime)
 * @property bool $isActive m:default(false)
 */
class Product extends BaseEntity
{

    public function initDefaults(): void {
        parent::initDefaults();
        $this->createDate = new DateTime();
    }

}
