<?php
/*
* @author Tom Hruby
* https://tomashruby.com
*/

namespace Model\Entity;

/**
 * @property-read int           $id m:schemaPrimary
 * @property      Account       $account m:hasOne(account_id:account)
 * @property      Organization  $organization m:hasOne(organization_id:organization)
 * @property      int           $role m:schemaType(tinyint) m:enum(\Rockette\Model\Enum\Role::ROLE_*) m:default(0)
 * @property      \DateTime|null $createDate m:schemaType(DateTime)
 *
 * @schemaUnique account_id, organization_id
 */
class AccountToOrganization extends BaseEntity
{

}
