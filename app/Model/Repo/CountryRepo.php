<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Model\Repo;

use Model\Entity\Country;

/**
 * @method Country getSingle($id)
 * @method Country[] getMultiple
 */
class CountryRepo extends BaseRepo
{

}
