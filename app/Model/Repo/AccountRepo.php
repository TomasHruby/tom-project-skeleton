<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Model\Repo;

use Model\Entity\Account;

/**
 * @method Account getSingle($id)
 * @method Account[] getMultiple
 */
class AccountRepo extends BaseRepo
{

}
