<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Model\Exception\Runtime;

use RuntimeException;

class EntityNotFound extends RuntimeException
{

}
